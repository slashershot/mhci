import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../modal/modal.component';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(public modalController : ModalController,public alertController: AlertController) {}

  faculty() { document.getElementById('faculty').style.visibility = "visible"; }
  field() { document.getElementById('field').style.visibility = "visible"; }
  univeristytower() { document.getElementById('univeristytower').style.visibility = "visible"; }
  
  facultyout() { document.getElementById('faculty').style.visibility = "hidden"; }
  fieldout() { document.getElementById('field').style.visibility = "hidden"; }
  univeristytowerout() { document.getElementById('univeristytower').style.visibility = "hidden"; }
  
    async presentModal() {
    const modal = await this.modalController.create({
      component: ModalComponent
    });
    return await modal.present();
  }
  
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }
  
}
