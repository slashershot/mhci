import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public items: any = [];
  constructor() {
          this.items = [
      { expanded: false,title: "Location 1 hint",content: "Over the ocean",extended_content:"under the sea" },
      { expanded: false,title: "Location 2 hint",content: "In my heart",extended_content:"under the sea" },
      { expanded: false,title: "Location 3 hint",content: "Fly above the skies",extended_content:"under the sea" },

    ];
  }
  
    expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }

}
